import {Core, Kore} from "@kirinnee/core";
import * as fs from "graceful-fs";
import * as path from "path";
import * as os from "os";

let core: Core = new Kore();
core.ExtendPrimitives();

const argv: string[] = process.argv.Skip(2);

main().then(e => process.exit(e));

function getPath(name: string): string {
	return path.join(os.homedir(), ".ssh/", name);
}

async function ReadFile(name: string): Promise<string> {
	return new Promise<string>(r => {
		const track = getPath(name);
		fs.readFile(track, 'utf8', (err, f) => {
			r(f)
		})
	});
}

async function WriteFile(name: string, content: string): Promise<void> {
	return new Promise<void>(r => {
		const track = getPath(name);
		fs.writeFile(track, content, 'utf8', () => r);
	});
}

function Exist(name: string): boolean {
	const track = getPath(name);
	return fs.existsSync(track);
}

async function main(): Promise<number> {
	if (argv.length !== 1) {
		console.log("Requires only 1 argument");
		return 1;
	} else {
		
		let target: string = argv[0];
		const REAL = ".~real_real_id_rsa";
		const FILE_NAME = "temp_state_file_name";
		const stateExist = Exist(FILE_NAME);
		if (!stateExist) {
			await WriteFile(FILE_NAME, REAL);
		}
		const current = await ReadFile(FILE_NAME);
		if (target === "id_rsa") target = REAL;
		if (target === current) {
			console.log("You are already using this SSH key");
			return 1;
		}
		const fullTarget = getPath(target);
		const fullCurrent = getPath(current);
		const realCurrent = getPath("id_rsa");
		fs.renameSync(realCurrent, fullCurrent);
		fs.renameSync(fullTarget, realCurrent);
		await WriteFile(FILE_NAME, target);
		return 0;
	}
}

